import { autoinject, BindingEngine } from "aurelia-framework";
import { ChildViewData } from "./child";

@autoinject
export class ParentWidget {
  viewData: ChildViewData[];

  constructor(private bindingEngine: BindingEngine) {
    this.viewData = [];
    let subscription = this.bindingEngine.collectionObserver(this.viewData)
        .subscribe(this.viewDataChanged.bind(this));
  }

  viewDataChanged(splices) {
    console.log("parent viewDataChanged", splices);
  }

  addEntry() {
    // This works because we exchange the whole array
    // const arr = [];
    // this.viewData.forEach((d) => arr.push(d));
    // arr.push({ label: 'Hi from Parent' });
    // this.viewData = arr;
    // Here the data is updated in child as expected but there the "viewdataChanged" of the child function is not triggered
    this.viewData.push({ label: "Hi from Parent" });
  }
}
