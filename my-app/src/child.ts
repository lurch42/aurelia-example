import { bindable, autoinject, BindingEngine } from "aurelia-framework";

@autoinject
export class Child {
  // Parent Bindings
  viewData: ChildViewData[];
  
  constructor(private bindingEngine: BindingEngine) {
  }

  activate(model) {
    this.viewData = model;
    let subscription = this.bindingEngine.collectionObserver(this.viewData)
        .subscribe(this.viewDataChanged.bind(this));
  }

  viewDataChanged(splices) {
    console.log("child viewDataChanged", splices);
  }

  addEntry() {
    const newEntry: ChildViewData = { label: "Hi from Child" };
    this.viewData.push(newEntry);
    console.log("child addEntry");
  }
}

export interface ChildViewData {
  label: string;
}
